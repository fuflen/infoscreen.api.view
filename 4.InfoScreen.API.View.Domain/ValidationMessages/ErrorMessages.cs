﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.ValidationMessages
{
    public static class ErrorMessages
    {

        //Endpoint URL
        public const string EndpointUrlNull = "The Endpoint Url is required and cannot be null.";
        public const string EndpointUrlEmpty = "Please enter a URL.";
        public const string EndpointUrlInvalid = "The given Endpoint Url is invalid. Please re-enter.";

        // Endpoint name
        public const string EndpointNameNull = "The Endpoint Name is required and cannot be null.";
        public const string EndpointNameEmpty = "Please enter a name for the Endpoint.";
        public const string EndpointNameLengthInvalid = "The name must be between 1 and 50 characters.";

        // id
        public const string IdNull = "The id cannot be null.";
        public const string IdInvalid = "The id needs to be higher than 0";

        // number of rows
        public const string NumberNull = "The number cannot be null.";
        public const string NumberToLow = "The number needs to be higher than 0";
        public const string NumberToHigh = "The number needs to be less than 50";
    }
}
