﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Result
{
    public interface IResult
    {
        bool HasError { get; }
        string Message { get; }

        [JsonIgnore]
        Exception Exception { get; }

        void ThrowOnError();
    }
}
