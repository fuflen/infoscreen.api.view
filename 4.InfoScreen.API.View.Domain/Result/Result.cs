﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Result
{
    public static class Result
    {
        public static Result<T> Success<T>(T data)
        {
            return new Result<T>()
            {
                Data = data,
                HasError = false,
            };
        }
        public static Result<T> Success<T>(T data, string message)
        {
            return new Result<T>()
            {
                Data = data,
                HasError = false,
                Message = message,
            };
        }
    }

    public struct Result<T> : IResult
    {
        public T Data { get; set; }
        public bool HasError { get; set; }
        public string Message { get; set; }

        [JsonIgnore]
        public Exception Exception { get; set; }

        public static Result<T> Success(T data)
        {
            return new Result<T>()
            {
                Data = data,
                HasError = false,
            };
        }

        public static Result<T> Error(string message)
        {
            return Error(null, message);
        }
        public static Result<T> Error(Exception exception)
        {
            string message = exception.Message;

            Exception innerException = exception.InnerException;
            while (innerException != null)
            {
                message += "\r\n" + innerException.Message;

                innerException = innerException.InnerException;
            }

            return Error(exception, message);
        }

        public static Result<T> Error(Exception exception, string message)
        {
            return new Result<T>()
            {
                Data = default(T),
                HasError = true,
                Message = message ?? "",
                Exception = exception,
            };
        }

        public Result<TNew> Convert<TNew>(TNew value)
        {
            return new Result<TNew>()
            {
                Data = value,
                Exception = this.Exception,
                HasError = this.HasError,
                Message = this.Message,
            };
        }

        public Result<TNew> ConvertToError<TNew>()
        {
            return new Result<TNew>()
            {
                Data = default(TNew),
                Exception = this.Exception,
                HasError = true,
                Message = this.Message,
            };
        }

        public void ThrowOnError()
        {
            if (HasError)
                throw new Exception(Message);
        }
    }
}
