﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
        bool Deleted { get; set; }
    }
}
