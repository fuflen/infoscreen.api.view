﻿using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.DTO.External
{
    public class TemplateDTO : DTOInterface
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HTMLContent { get; set; }
    }
}
