﻿using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.DTO
{
    public class ViewDTO : DTOInterface
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EndpointId { get; set; }
        public int TemplateId { get; set; }
        public int NumberOfRows { get; set; }
        public int RefreshDataIntervalInMinutes { get; set; }
    }
}
