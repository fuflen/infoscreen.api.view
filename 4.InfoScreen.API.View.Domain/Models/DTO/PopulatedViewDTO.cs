﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.DTO
{
    public class PopulatedViewDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ViewContent { get; set; }
    }
}
