﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;

namespace _4.InfoScreen.API.View.Domain.Models.RabbitMQModels
{
    public class RabbitMQConnectionModel
    {
        public IModel Model { get; set; }
        public IBasicProperties Properties { get; set; }
        public string ExchangeName { get; set; }
    }
}
