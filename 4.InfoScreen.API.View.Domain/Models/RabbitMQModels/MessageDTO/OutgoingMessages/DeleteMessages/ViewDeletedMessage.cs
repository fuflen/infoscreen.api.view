﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteMessages
{
    public class ViewDeletedMessage
    {
        public int ViewId { get; set; }
        public int EndpointId { get; set; }
        public int TemplateId { get; set; }
    }
}
