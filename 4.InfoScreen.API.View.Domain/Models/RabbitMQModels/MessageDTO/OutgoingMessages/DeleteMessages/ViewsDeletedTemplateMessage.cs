﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteMessages
{
    public class ViewsDeletedTemplateMessage
    {
        public List<int> ViewIds { get; set; }
        public int TemplateId { get; set; }
    }
}
