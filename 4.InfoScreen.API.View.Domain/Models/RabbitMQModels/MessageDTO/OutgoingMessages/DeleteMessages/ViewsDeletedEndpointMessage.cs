﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO
{
    public class ViewsDeletedEndpointMessage
    {
        public List<int> ViewIds { get; set; }
        public int EndpointId { get; set; }
    }
}
