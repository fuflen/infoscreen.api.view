﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteFailedMessages
{
    public class ViewDeleteFailedEndpointMessage
    {
        public int EndpointId { get; set; }

    }
}
