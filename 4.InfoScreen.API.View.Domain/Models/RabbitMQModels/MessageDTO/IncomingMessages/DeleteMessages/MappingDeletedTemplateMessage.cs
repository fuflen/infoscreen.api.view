﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.IncomingMessages.DeleteMessages
{
    public class MappingDeletedTemplateMessage
    {
        public int TemplateId { get; set; }

    }
}
