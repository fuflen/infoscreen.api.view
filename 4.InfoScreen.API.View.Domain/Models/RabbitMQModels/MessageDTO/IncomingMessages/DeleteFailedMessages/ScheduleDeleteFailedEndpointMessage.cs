﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.IncomingMessages.DeleteFailedMessages
{
    public class ScheduleDeleteFailedEndpointMessage
    {
        public List<int> ViewIds { get; set; }
        public int EndpointId { get; set; }
    }
}
