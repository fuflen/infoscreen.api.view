﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using _3.InfoScreen.API.View.Infrastructure.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.RabbitMQModels;
using _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO;
using _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.IncomingMessages.DeleteMessages;
using System.Linq;
using _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.IncomingMessages.DeleteFailedMessages;

namespace _3.InfoScreen.API.View.Infrastructure.HostedService
{
    public class RabbitMQHostedService : IHostedService
    {
        private IServiceProvider _services;
        private RabbitMQConnectionModel _connectionModel;

        private const string DeleteQueue = "delete_views";

        private const string MappingDeletedEndpointBindingKey = "mapping.deleted.endpoint";
        private const string MappingDeletedTemplateBindingKey = "mapping.deleted.template";

        private const string UndoDeleteQueue = "undo_delete_view";

        private const string ScheduleDeleteFailedEndpointBindingKey = "schedule.delete.failed.endpoint";
        private const string ScheduleDeleteFailedTemplateBindingKey = "schedule.delete.failed.template";
        private const string MappingDeleteFailedViewBindingKey = "mapping.delete.failed.view";


        public RabbitMQHostedService(RabbitMQConnectionService connection, IServiceProvider services)
        {
            Console.WriteLine("HOSTED SERVICE STARTING!!!");
            _services = services;
            _connectionModel = connection.GetConnectionModel();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            ConfiguredDeleteQueue();


        }

        private void ConfiguredDeleteQueue()
        {
            Console.WriteLine("START ASYNC!!!");

            _connectionModel.Model.QueueDeclare(DeleteQueue, true, false, false, null);

            Console.WriteLine("QUEUE DECLARED!!!");

            _connectionModel.Model.QueueBind(DeleteQueue, _connectionModel.ExchangeName, MappingDeletedEndpointBindingKey);
            _connectionModel.Model.QueueBind(DeleteQueue, _connectionModel.ExchangeName, MappingDeletedTemplateBindingKey);

            var consumer = new EventingBasicConsumer(_connectionModel.Model);

            Console.WriteLine("CONSUMER INSTANTIATED!!!");

            consumer.Received += async (model, ea) =>
            {
                await HandleReceivedEvent(ea);
                var body = ea.Body;
                var routingKey = ea.RoutingKey;
                var message = Encoding.UTF8.GetString(body);

                Console.WriteLine(message + " from ViewService");

            };

            Console.WriteLine("CONSOLE METHOD WRITELINE");

            _connectionModel.Model.BasicConsume(queue: DeleteQueue,
                autoAck: true,
                consumer: consumer);
        }

        private void ConfigureUndoDeleteQueue()
        {
            _connectionModel.Model.QueueDeclare(UndoDeleteQueue, true, false, false, null);

            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, ScheduleDeleteFailedEndpointBindingKey);
            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, ScheduleDeleteFailedTemplateBindingKey);
            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, MappingDeleteFailedViewBindingKey);

            var consumer = new EventingBasicConsumer(_connectionModel.Model);

            Console.WriteLine("CONSUMER INSTANTIATED!!!");

            consumer.Received += async (model, ea) =>
            {
                await HandleReceivedUndoEventAsync(ea);
                var body = ea.Body;
                var routingKey = ea.RoutingKey;
                var message = Encoding.UTF8.GetString(body);

                Console.WriteLine(message + " from ViewService");

            };

            Console.WriteLine("CONSOLE METHOD WRITELINE");

            _connectionModel.Model.BasicConsume(queue: UndoDeleteQueue,
                autoAck: true,
                consumer: consumer);
        }

        

        private async Task HandleReceivedEvent(BasicDeliverEventArgs ea)
        {
            Console.WriteLine("Handler started");

            switch (ea.RoutingKey)
            {
                case MappingDeletedEndpointBindingKey:
                    MappingDeletedEndpointMessage endpointMessage = Deserialize<MappingDeletedEndpointMessage>(ea.Body);
                    await DeleteViewsWithEndpoint(endpointMessage.EndpointId);
                    break;
                case MappingDeletedTemplateBindingKey:
                    Console.WriteLine("Converting JSON");
                    MappingDeletedTemplateMessage viewMessage = Deserialize<MappingDeletedTemplateMessage>(ea.Body);
                    await DeleteViewsWithTemplateId(viewMessage.TemplateId);
                    break;
            }

        }
        private async Task HandleReceivedUndoEventAsync(BasicDeliverEventArgs ea)
        {
            Console.WriteLine("Handler started");

            switch (ea.RoutingKey)
            {
                case ScheduleDeleteFailedEndpointBindingKey:
                    ScheduleDeleteFailedEndpointMessage endpoingMessage = Deserialize<ScheduleDeleteFailedEndpointMessage>(ea.Body);
                    await UndoDeleteViewsWithEndpoint(endpoingMessage.EndpointId);
                    break;
                case ScheduleDeleteFailedTemplateBindingKey:
                    Console.WriteLine("Converting JSON");
                    ScheduleDeleteFailedTemplateMessage ScheduleMessage = Deserialize<ScheduleDeleteFailedTemplateMessage>(ea.Body);
                    await UndoDeleteViewsTemplateWithTemplate(ScheduleMessage.TemplateId);
                    break;
                case MappingDeleteFailedViewBindingKey:
                    Console.WriteLine("Converting JSON");
                    MappingDeleteFailedViewMessage scheduleMessage = Deserialize<MappingDeleteFailedViewMessage>(ea.Body);
                    await UndoDeleteView(scheduleMessage.EndpointId, scheduleMessage.TemplateId, scheduleMessage.ViewId);
                    break;
            }
        }

        

        public static T Deserialize<T>(byte[] data) where T : class
        {
            using (var stream = new MemoryStream(data))
            using (var reader = new StreamReader(stream, Encoding.UTF8))
                return JsonSerializer.Create().Deserialize(reader, typeof(T)) as T;
        }

        private async Task DeleteViewsWithEndpoint(int endpointId)
        {
            Console.WriteLine("Deleting views With Endpoint: " + endpointId);
            using (var scope = _services.CreateScope())
            {
                var viewRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IViewRepository>();

                var messageService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQViewService>();

                try
                {
                    var entities = await viewRepo.DeleteManyByEndpointId(endpointId);

                    var ids = entities.Select(x => x.Id);
                    foreach (var entity in entities)
                    {
                        messageService.SendViewDeletedFromEndpointMessage(ids.ToList(), endpointId);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                    messageService.SendViewDeleteFailedFromEndpointMessage(endpointId);
                }
            }
        }

        private async Task DeleteViewsWithTemplateId(int templateId)
        {
            Console.WriteLine("Deleting views With TemplateId: " + templateId);
            using (var scope = _services.CreateScope())
            {
                var viewRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IViewRepository>();

                var messageService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQViewService>();


                try
                {
                    var entities = await viewRepo.DeleteManyByTemplateId(templateId);

                    var ids = entities.Select(x => x.Id);
                    foreach (var entity in entities)
                    {
                        messageService.SendViewDeletedFromTemplateMessage(ids.ToList(), templateId);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                    messageService.SendViewDeleteFailedFromTemplateMessage(templateId);
                }
            }
        }


        private async Task UndoDeleteViewsWithEndpoint(int endpointId)
        {
            Console.WriteLine("Undoin Deleting View With Endpoint: " + endpointId);
            try
            {
                using (var scope = _services.CreateScope())
                {
                    var viewRepo =
                        scope.ServiceProvider
                            .GetRequiredService<IViewRepository>();

                    var messagingService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQViewService>();


                    await viewRepo.UndoDeleteManyByEndpointId(endpointId);
                    messagingService.SendViewDeleteFailedFromEndpointMessage(endpointId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        private async Task UndoDeleteViewsTemplateWithTemplate(int templateId)
        {
            Console.WriteLine("Undoing Deleting View With Template: " + templateId);
            try
            {
                using (var scope = _services.CreateScope())
                {
                    var viewRepo =
                        scope.ServiceProvider
                            .GetRequiredService<IViewRepository>();

                    var messagingService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQViewService>();


                    await viewRepo.UndoDeleteManyByTemplateId(templateId);
                    messagingService.SendViewDeleteFailedFromTemplateMessage(templateId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        private async Task UndoDeleteView(int endpointId, int templateId, int viewId)
        {
            Console.WriteLine("Undoing Deleting View With Template: " + templateId);
            try
            {
                using (var scope = _services.CreateScope())
                {
                    var viewRepo =
                        scope.ServiceProvider
                            .GetRequiredService<IViewRepository>();

                    var messagingService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQViewService>();


                    await viewRepo.UndoDelete(viewId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }



        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                _connectionModel.Model.Dispose();
                cancellationToken.ThrowIfCancellationRequested();
            });
        }
    }
}
