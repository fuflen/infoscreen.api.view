﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.View.Infrastructure.Context
{
    public interface IDbInitializer
    {
        void Initialize(ViewContext context);
    }
}
