﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _4.InfoScreen.API.View.Domain.Models.Entities;

namespace _3.InfoScreen.API.View.Infrastructure.Context
{
    public class DbInitializer : IDbInitializer
    {
        public void Initialize(ViewContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            if (context.Views.Any())
            {
                return;
            }

            List<ViewEntity> dataEndpoints = new List<ViewEntity>
            {
                new ViewEntity{ EndpointId = 1, TemplateId = 1, NumberOfRows = 10, RefreshDataIntervalInMinutes = 120, Name = "Card View - Startwars People" },
                new ViewEntity{ EndpointId = 2, TemplateId = 1, NumberOfRows = 10, RefreshDataIntervalInMinutes = 120, Name = "Card View - Starwars Planets" },
                new ViewEntity{ EndpointId = 3, TemplateId = 1, NumberOfRows = 10, RefreshDataIntervalInMinutes = 120, Name = "Card View - Random Endpoint Data" },
                new ViewEntity{ EndpointId = 5, TemplateId = 1, NumberOfRows = 10, RefreshDataIntervalInMinutes = 120, Name = "Card View - Starwars Species" }
            };

            context.Views.AddRange(dataEndpoints);
            context.SaveChanges();
        }
    }
}
