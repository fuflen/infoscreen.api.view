﻿using _4.InfoScreen.API.View.Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.View.Infrastructure.Context
{
    public class ViewContext : DbContext
    {
        /// <summary>
        /// Constructor for <see cref="IntegrationContext"/>
        /// </summary>
        /// <param name="options">The <see cref="DbContextOptions"/></param>
        public ViewContext(DbContextOptions<ViewContext> options)
           : base(options)
        {
        }

        public DbSet<ViewEntity> Views { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Building DB model for DataEndpoint entity
            builder.Entity<ViewEntity>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).ForSqlServerUseSequenceHiLo();
                entity.Property(x => x.EndpointId).IsRequired();
                entity.Property(x => x.TemplateId).IsRequired();
                entity.Property(x => x.NumberOfRows).IsRequired();
                entity.Property(x => x.Name).IsRequired();
            });
        }
    }
}
