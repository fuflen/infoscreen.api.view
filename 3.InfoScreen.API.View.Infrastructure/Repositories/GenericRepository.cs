﻿using _3.InfoScreen.API.View.Infrastructure.Context;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.View.Infrastructure.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
   where TEntity : class, IEntity
    {
        private readonly ViewContext _dbContext;

        /// <summary>
        /// The constructor for the Generic Repository
        /// </summary>
        /// <param name="dbContext">The context to use. </param>
        public GenericRepository(ViewContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Saved the given Entity to its corresponding DbSet table. 
        /// </summary>
        /// <param name="entity"></param>
        /// <return id="int">The created ID</return>
        /// <returns></returns>
        public async Task<TEntity> Create(TEntity entity)
        {
            entity.Deleted = false;
            await _dbContext.Set<TEntity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        /// <summary>
        /// Deleted the Entity with the given id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> Delete(int id)
        {
            var entity = await GetById(id);
            entity.Deleted = true;

            var updatedEntity = _dbContext.Set<TEntity>().Update(entity).Entity;

            await _dbContext.SaveChangesAsync();

            return updatedEntity;
        }

        /// <summary>
        /// Undoing the deletion of the Entity with the given id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> UndoDelete(int id)
        {
            var entity = await GetById(id);
            entity.Deleted = false;

            var updatedEntity = _dbContext.Set<TEntity>().Update(entity).Entity;

            await _dbContext.SaveChangesAsync();

            return updatedEntity;
        }

        /// <summary>
        /// Get the whole DbSet for the Entity.
        /// AsNoTracking as it improved performance. And Eliminated Reference loops. 
        /// </summary>
        /// <returns>IQuerable of <see cref="TEntity"/> so they can be further queued </returns>
        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().Where(x => !x.Deleted).AsNoTracking();
        }

        /// <summary>
        /// Gets the Entity with the given id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> GetById(int id)
        {
            return await _dbContext.Set<TEntity>()
                        .AsNoTracking()
                        .FirstAsync(e => e.Id == id && !e.Deleted);
        }

        public async Task<TEntity> Update(int id, TEntity entity)
        {
            var oldEntity = await GetById(id);
            entity.Deleted = oldEntity.Deleted;

            var updatedEntity = _dbContext.Set<TEntity>().Update(entity).Entity;

            await _dbContext.SaveChangesAsync();

            return updatedEntity;
        }
    }
}
