﻿using _3.InfoScreen.API.View.Infrastructure.Context;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.Entities;
using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace _3.InfoScreen.API.View.Infrastructure.Repositories.EntityRepositories
{
    public class ViewRepository : GenericRepository<ViewEntity>, IViewRepository
    {
        private ViewContext _dbContext;
        public ViewRepository(ViewContext dbContext)
        : base(dbContext)
        {
            _dbContext = dbContext;
            dbContext.Database.EnsureCreated();
        }

        public async Task<ViewEntity> GetByEndpointIdAndTemplateId(int endpointId, int templateId)
        {
            return await _dbContext.Set<ViewEntity>()
                                   .AsNoTracking()
                                   .FirstAsync(e => e.EndpointId == endpointId && e.TemplateId == templateId);
        }

        private async Task<List<ViewEntity>> GetManyByEndpointId(int endpointId)
        {
            return await _dbContext.Set<ViewEntity>().Where(x => x.EndpointId == endpointId).ToListAsync();

        }

        public async Task<List<ViewEntity>> DeleteManyByEndpointId(int endpointId)
        {
            var entitiesToRemove = await GetManyByEndpointId(endpointId);
            entitiesToRemove.ForEach(x => x.Deleted = true);
            _dbContext.Set<ViewEntity>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        private async Task<List<ViewEntity>> GetManyByTemplateId(int templateId)
        {
            return await _dbContext.Set<ViewEntity>().Where(x => x.TemplateId == templateId).ToListAsync();

        }

        public async Task<List<ViewEntity>> DeleteManyByTemplateId(int templateId)
        {
            var entitiesToRemove = await GetManyByTemplateId(templateId);
            entitiesToRemove.ForEach(x => x.Deleted = true);
            _dbContext.Set<ViewEntity>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        public async Task<List<ViewEntity>> UndoDeleteManyByEndpointId(int endpointId)
        {
            var entitiesToRemove = await GetManyByEndpointId(endpointId);
            entitiesToRemove.ForEach(x => x.Deleted = false);
            _dbContext.Set<ViewEntity>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        public async Task<List<ViewEntity>> UndoDeleteManyByTemplateId(int templateId)
        {
            var entitiesToRemove = await GetManyByTemplateId(templateId);
            entitiesToRemove.ForEach(x => x.Deleted = false);
            _dbContext.Set<ViewEntity>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }
    }

    
}
