﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _3.InfoScreen.API.View.Infrastructure.Services.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Models.Entities;
using _4.InfoScreen.API.View.Domain.Result;

namespace _3.InfoScreen.API.View.Infrastructure.Managers
{
    public class DataManager
    {
        private ITemplateService _templateService;
        private IDistributedCache _cache;
        private IViewRepository _repo;

        public DataManager(
            IViewRepository repo,
            ITemplateService templateService,
            IDistributedCache cache)
        {
            _repo = repo;
            _templateService = templateService;
            _cache = cache;
        }

        public async Task<Result<PopulatedViewDTO>> GetPopulatedViewAsync(int id)
        {

            var populatedViewDTO = new PopulatedViewDTO();

            var cachedViewDto = await _cache.GetStringAsync(id.ToString());
            

            if(string.IsNullOrEmpty(cachedViewDto))
            {
                var view = await _repo.GetById(id);

                var populatedTemplate =
                    await _templateService.GetPopulatedTemplate(view.EndpointId, view.TemplateId, view.NumberOfRows);

                populatedViewDTO = new PopulatedViewDTO()
                    {Id = view.Id, Name = view.Name, ViewContent = populatedTemplate.Data.HTMLContent};


                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(view.RefreshDataIntervalInMinutes))
                    .SetSlidingExpiration(TimeSpan.FromHours(24));

                await _cache.SetStringAsync(id.ToString(), JsonConvert.SerializeObject(populatedViewDTO), options);
            }
            else
            {
                populatedViewDTO = JsonConvert.DeserializeObject<PopulatedViewDTO>(cachedViewDto);
            }

            return Result<PopulatedViewDTO>.Success(populatedViewDTO);
        }
    }
}
