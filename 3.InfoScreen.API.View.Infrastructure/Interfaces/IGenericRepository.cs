﻿using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.View.Infrastructure.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class, IEntity
    {
        IQueryable<TEntity> GetAll();

        Task<TEntity> GetById(int id);

        Task<TEntity> Create(TEntity entity);

        Task<TEntity> Update(int id, TEntity entity);

        Task<TEntity> Delete(int id);

        Task<TEntity> UndoDelete(int id);
    }
}
