﻿using _4.InfoScreen.API.View.Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.View.Infrastructure.Interfaces
{
    public interface IViewRepository : IGenericRepository<ViewEntity>
    {
        Task<ViewEntity> GetByEndpointIdAndTemplateId(int endpointId, int templateId);
        Task<List<ViewEntity>> DeleteManyByEndpointId(int endpointId);
        Task<List<ViewEntity>> DeleteManyByTemplateId(int templateId);

        Task<List<ViewEntity>> UndoDeleteManyByEndpointId(int endpointId);

        Task<List<ViewEntity>> UndoDeleteManyByTemplateId(int templateId);
    }
}
