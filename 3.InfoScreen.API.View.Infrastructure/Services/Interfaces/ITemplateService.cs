﻿using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Models.DTO.External;
using _4.InfoScreen.API.View.Domain.Result;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.View.Infrastructure.Services.Interfaces
{
    public interface ITemplateService
    {
        Task<Result<TemplateDTO>> GetPopulatedTemplate(int endpointId, int templateId, int numberOfRows);

    }
}
