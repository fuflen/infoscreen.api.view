﻿using _3.InfoScreen.API.View.Infrastructure.Services.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Models.DTO.External;
using _4.InfoScreen.API.View.Domain.Result;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.View.Infrastructure.Services
{
    public class TemplateService : ITemplateService
    {
        private HttpClient _client;
        private string mappingServiceUrl = "http://templateservice/api/PopulatedTemplate/GetPopulatedTemplate";
        public TemplateService(HttpClient client)
        {
            this._client = client;
        }


        public async Task<Result<TemplateDTO>> GetPopulatedTemplate(int endpointId, int templateId, int numberOfRows)
        {
            HttpResponseMessage response = await _client.GetAsync(mappingServiceUrl + "/" + templateId + "/" + endpointId + "/" + numberOfRows);

            var dto = await response.Content.ReadAsAsync<Result<TemplateDTO>>();

            return dto;
        }
    }
}
