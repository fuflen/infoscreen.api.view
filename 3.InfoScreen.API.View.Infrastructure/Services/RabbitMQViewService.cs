﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Models.Entities;
using _4.InfoScreen.API.View.Domain.Models.RabbitMQModels;
using _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO;
using _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteMessages;
using _4.InfoScreen.API.View.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteFailedMessages;

namespace _3.InfoScreen.API.View.Infrastructure.Services
{
    public class RabbitMQViewService
    {
        private static RabbitMQConnectionModel _connectionModel;

        private const string ViewDeletedEndpointRoutingKey = "view.deleted.endpoint";
        private const string ViewDeletedTemplateRoutingKey = "view.deleted.endpoint";
        private const string ViewDeletedRoutingKey = "view.deleted";

        private const string ViewDeleteFailedEndpointRoutingKey = "view.delete.failed.endpoint";
        private const string ViewDeleteFailedTemplateRoutingKey = "view.delete.failed.template";
  

        public RabbitMQViewService(RabbitMQConnectionService connection)
        {
            _connectionModel = connection.GetConnectionModel();
        }

        public void SendViewDeletedFromEndpointMessage(List<int> ids, int endpointId)
        {
            var messageModel = new ViewsDeletedEndpointMessage()
            {
                ViewIds = ids,
                EndpointId = endpointId,
            };
            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messageModel));
            SendMessage(body, ViewDeletedEndpointRoutingKey);
        }

        public void SendViewDeletedFromTemplateMessage(List<int> ids ,int templateId)
        {
            var messageModel = new ViewsDeletedTemplateMessage()
            {
                ViewIds = ids,
                TemplateId = templateId,
            };
            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messageModel));
            SendMessage(body, ViewDeletedTemplateRoutingKey);
        }

        public void SendViewDeletedMessage(ViewEntity view)
        {
            var messageModel = new ViewDeletedMessage()
            {
                ViewId = view.Id,
                EndpointId = view.EndpointId,
                TemplateId = view.TemplateId,
            };
            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messageModel));
            SendMessage(body, ViewDeletedRoutingKey);
        }

        public void SendViewDeleteFailedFromEndpointMessage(int endpointId)
        {
            var messageModel = new ViewDeleteFailedEndpointMessage()
            {
                EndpointId = endpointId,
            };
            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messageModel));
            SendMessage(body, ViewDeleteFailedEndpointRoutingKey);
        }

        public void SendViewDeleteFailedFromTemplateMessage(int templateId)
        {
            var messageModel = new ViewsDeleteFailedTemplateMessage()
            {
                TemplateId = templateId,
            };
            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messageModel));
            SendMessage(body, ViewDeleteFailedTemplateRoutingKey);
        }

        private void SendMessage(byte[] message, string routingKey)
        {
            _connectionModel.Model.BasicPublish(_connectionModel.ExchangeName, routingKey, _connectionModel.Properties, message);
        }
    }
}
