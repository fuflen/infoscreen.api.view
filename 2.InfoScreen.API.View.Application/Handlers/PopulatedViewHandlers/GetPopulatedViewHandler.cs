﻿using _2.InfoScreen.API.View.Application.Commands.PopulatedViewCommands;
using _2.InfoScreen.API.View.Application.Commands.ViewCommands;
using _2.InfoScreen.API.View.Application.Validators.Interfaces;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _3.InfoScreen.API.View.Infrastructure.Services.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Models.Entities;
using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using _4.InfoScreen.API.View.Domain.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using _3.InfoScreen.API.View.Infrastructure.Managers;

namespace _2.InfoScreen.API.View.Application.Handlers.PopulatedViewHandlers
{
    public class GetPopulatedViewHandler : IRequestHandler<GetPopulatedViewCommand, Result<PopulatedViewDTO>>
    {
        private IValidateManager<ViewDTO> _validateManager;
        private ILogger _logger;
        private DataManager _dataManager;

        public GetPopulatedViewHandler(
            IValidateManager<ViewDTO> validateManager,
            ILogger<GetPopulatedViewHandler> logger,
            DataManager dataManager)
        {
            _validateManager = validateManager;
            _logger = logger;
            _dataManager = dataManager;
        }


        public async Task<Result<PopulatedViewDTO>> Handle(GetPopulatedViewCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateGetByIdRequest(command.RequestModel);

                Result<PopulatedViewDTO> result = await _dataManager.GetPopulatedViewAsync(command.RequestModel.Id);

                return result;
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<PopulatedViewDTO>.Error(e);
            }
        }
    }
}
