﻿using _2.InfoScreen.API.View.Application.Commands.ViewCommands;
using _2.InfoScreen.API.View.Application.Validators.Interfaces;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Models.Entities;
using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using _4.InfoScreen.API.View.Domain.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _3.InfoScreen.API.View.Infrastructure.Services;

namespace _2.InfoScreen.API.View.Application.Handlers.ViewHandlers
{
    public class GetAllViewHandler : IRequestHandler<GetAllViewCommand, Result<List<ViewDTO>>>
    {
        private IViewRepository _repo;
        private IMapper _mapper;
        private ILogger _logger;
        private RabbitMQViewService _messageService;

        public GetAllViewHandler(
            IViewRepository repo,
            IMapper mapper,
            ILogger<GetAllViewHandler> logger,
            RabbitMQViewService messageService)
        {
            _repo = repo;
            _mapper = mapper;
            _logger = logger;
            _messageService = messageService;
        }

        public async Task<Result<List<ViewDTO>>> Handle(GetAllViewCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var endpoints = await Task.Factory.StartNew(() => _repo.GetAll().ToList());
                var endpointDtos = _mapper.Map<List<ViewDTO>>(endpoints);
                return Result<List<ViewDTO>>.Success(endpointDtos);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<List<ViewDTO>>.Error(e);
            }
        }

    }
}
