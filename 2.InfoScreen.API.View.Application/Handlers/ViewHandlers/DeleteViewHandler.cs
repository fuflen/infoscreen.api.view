﻿using _2.InfoScreen.API.View.Application.Commands.ViewCommands;
using _2.InfoScreen.API.View.Application.Validators.Interfaces;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Models.Entities;
using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using _4.InfoScreen.API.View.Domain.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _3.InfoScreen.API.View.Infrastructure.Services;

namespace _2.InfoScreen.API.View.Application.Handlers.ViewHandlers
{
    class DeleteViewHandler : IRequestHandler<DeleteViewCommand, Result<ViewDTO>>
    {
        private IViewRepository _repo;
        private IMapper _mapper;
        private IValidateManager<ViewDTO> _validateManager;
        private ILogger _logger;
        private RabbitMQViewService _messageService;

        public DeleteViewHandler(
            IViewRepository repo,
            IMapper mapper,
            IValidateManager<ViewDTO> validateManager,
            ILogger<DeleteViewHandler> logger,
            RabbitMQViewService messageService)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _logger = logger;
            _messageService = messageService;
        }

        public async Task<Result<ViewDTO>> Handle(DeleteViewCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateDeleteRequest(command.RequestModel);
                ViewEntity entity = await _repo.Delete(command.RequestModel.Id);
                _messageService.SendViewDeletedMessage(entity);
                var dto = _mapper.Map<ViewDTO>(entity);
                return Result<ViewDTO>.Success(dto);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<ViewDTO>.Error(e);
            }
        }
    }
}
