﻿using _2.InfoScreen.API.View.Application.Commands.ViewCommands;
using _2.InfoScreen.API.View.Application.Validators.Interfaces;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Models.Entities;
using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using _4.InfoScreen.API.View.Domain.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _2.InfoScreen.API.View.Application.Handlers.ViewHandlers
{
    public class GetByEndpointIdAndTemplateIdHandler : IRequestHandler<GetByEndpointIdAndTemplateIdCommand, Result<ViewDTO>>
    {
        private IViewRepository _repo;
        private IMapper _mapper;
        private IValidateManager<ViewDTO> _validateManager;
        private ILogger _logger;

        public GetByEndpointIdAndTemplateIdHandler(
            IViewRepository repo,
            IMapper mapper,
            IValidateManager<ViewDTO> validateManager,
            ILogger<GetByEndpointIdAndTemplateIdHandler> logger)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _logger = logger;
        }

        public async Task<Result<ViewDTO>> Handle(GetByEndpointIdAndTemplateIdCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateGetByEndpointIdAndTemplateIdRequest(command.RequestModel);
                var endpoint = await _repo.GetByEndpointIdAndTemplateId(command.RequestModel.EndpointId, command.RequestModel.TemplateId);
                var dto = _mapper.Map<ViewDTO>(endpoint);
                return Result<ViewDTO>.Success(dto);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<ViewDTO>.Error(e);
            }
        }
    }
}
