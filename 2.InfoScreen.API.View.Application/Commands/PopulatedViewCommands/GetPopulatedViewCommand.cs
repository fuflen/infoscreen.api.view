﻿using _2.InfoScreen.API.View.Application.Requests.CRUD;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.View.Application.Commands.PopulatedViewCommands
{
    public class GetPopulatedViewCommand : IRequest<Result<PopulatedViewDTO>>
    {
        public GetPopulatedViewCommand(GetByIdRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetByIdRequest RequestModel { get; set; }
    }
}
