﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.View.Application.Requests.CRUD
{
    public class GetByEndpointIdAndTemplateIdRequest
    {
        public int EndpointId { get; set; }
        public int TemplateId { get; set; }
    }
}
