﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.View.Application.Requests.CRUD
{
    public class DeleteRequest
    {
        public int Id { get; set; }
    }
}
