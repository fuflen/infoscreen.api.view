﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;

namespace _2.InfoScreen.API.View.Application.CustomHealthChecks
{
    public class MemoryHealthCheck : IHealthCheck
    {
        private readonly IOptionsMonitor<MemoryCheckOptions> _options;

        public MemoryHealthCheck(IOptionsMonitor<MemoryCheckOptions> options)
        {
            _options = options;
        }

        /// <summary>
        /// Checking memory consumption od the service. If the memory consumption exceeds the threshold of approx. 1GB the service
        /// will be flagged "unhealthy".
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>A <see cref="HealthCheckResult"/> with HealthStatus Enum stating if the service is Healthy or Unhealthy/></returns>
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var options = _options.Get(context.Registration.Name);

            var allocated = GC.GetTotalMemory(forceFullCollection: false);
            var data = new Dictionary<string, object>()
            {
                {"AllocatedBytes", allocated},
                {"Threshold", options.Threshold}
            };

            var status = (allocated < options.Threshold) ? HealthStatus.Healthy : HealthStatus.Unhealthy;

            return Task.FromResult(new HealthCheckResult(
                status,
                description: "Reports unhealthy status if allocated bytes " +
                             $">= {options.Threshold} bytes.",
                exception: null,
                data: data));
        }

        /// <summary>
        /// The memory threshold.
        /// </summary>
        public class MemoryCheckOptions
        {
            // Failure threshold (in bytes)
            public long Threshold { get; set; } = 1024L * 1024L * 256L;
        }
    }
}
