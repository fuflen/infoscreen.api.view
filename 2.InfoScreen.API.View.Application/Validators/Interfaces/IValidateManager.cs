﻿using _2.InfoScreen.API.View.Application.Requests.CRUD;
using _4.InfoScreen.API.View.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.View.Application.Validators.Interfaces
{
    public interface IValidateManager<T> where T : DTOInterface
    {
        void ValidateCreateRequest(CreateRequest<T> request);
        void ValidateUpdateRequest(UpdateRequest<T> request);
        void ValidateGetByIdRequest(GetByIdRequest request);
        void ValidateGetByEndpointIdAndTemplateIdRequest(GetByEndpointIdAndTemplateIdRequest request);
        void ValidateDeleteRequest(DeleteRequest request);
        
    }
}
