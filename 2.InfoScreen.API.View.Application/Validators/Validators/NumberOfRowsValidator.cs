﻿using _4.InfoScreen.API.View.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.View.Application.Validators.Validators
{
    class NumberOfRowsValidator : AbstractValidator<int>
    {
        public NumberOfRowsValidator()
        {
            RuleFor(x => x).
                            NotNull().
                            WithMessage(ErrorMessages.NumberNull).
                            GreaterThan(0).
                            WithMessage(ErrorMessages.NumberToLow).
                            LessThan(50).
                            WithMessage(ErrorMessages.NumberToHigh);
        }
    }
}
