﻿using _4.InfoScreen.API.View.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.View.Application.Validators.Validators
{
    class ViewNameValidator : AbstractValidator<string>
    {
        public ViewNameValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.EndpointNameNull).
                NotEmpty().
                WithMessage(ErrorMessages.EndpointNameEmpty).
                Must(BeValidNameLength).
                WithMessage(ErrorMessages.EndpointNameLengthInvalid);
        }

        private static bool BeValidNameLength(string name)
        {
            return name != null && name.Length >= 1 && name.Length <= 50;
        }
    }
}
