﻿using _4.InfoScreen.API.View.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.View.Application.Validators.Validators
{
    public class ViewIdValidator : AbstractValidator<int>
    {
        public ViewIdValidator()
        {
            RuleFor(x => x).
                            NotNull().
                            WithMessage(ErrorMessages.IdNull).
                            GreaterThan(0).
                            WithMessage(ErrorMessages.IdInvalid);
        }
    }
}
