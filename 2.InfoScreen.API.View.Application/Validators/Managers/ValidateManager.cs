﻿using _2.InfoScreen.API.View.Application.Requests.CRUD;
using _2.InfoScreen.API.View.Application.Validators.Interfaces;
using _2.InfoScreen.API.View.Application.Validators.Validators;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.View.Application.Validators.Managers
{
    public class ValidateManager : IValidateManager<ViewDTO>
    {
        /// <see cref="IValidator"/>
        private readonly IValidator<string> _nameValidator;
        private readonly IValidator<int> _idValidator;
        private readonly IValidator<int> _numberOfRowValidator;

        public ValidateManager()
        {
            _nameValidator = new ViewNameValidator();
            _idValidator = new ViewIdValidator();
            _numberOfRowValidator = new NumberOfRowsValidator();
        }

        public void ValidateCreateRequest(CreateRequest<ViewDTO> request)
        {
            _numberOfRowValidator.ValidateAndThrow(request.dto.NumberOfRows);
            _nameValidator.ValidateAndThrow(request.dto.Name);
            _idValidator.ValidateAndThrow(request.dto.TemplateId);
            _idValidator.ValidateAndThrow(request.dto.EndpointId);
        }

        public void ValidateDeleteRequest(DeleteRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateGetByIdRequest(GetByIdRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }
        public void ValidateGetByEndpointIdAndTemplateIdRequest(GetByEndpointIdAndTemplateIdRequest request)
        {
            _idValidator.ValidateAndThrow(request.EndpointId);

            _idValidator.ValidateAndThrow(request.TemplateId);
        }

        public void ValidateUpdateRequest(UpdateRequest<ViewDTO> request)
        {
            _numberOfRowValidator.ValidateAndThrow(request.dto.NumberOfRows);
            _nameValidator.ValidateAndThrow(request.dto.Name);
            _idValidator.ValidateAndThrow(request.dto.TemplateId);
            _idValidator.ValidateAndThrow(request.dto.EndpointId);
        }

    }
}
