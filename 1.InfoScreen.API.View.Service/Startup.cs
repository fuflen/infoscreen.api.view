﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using _2.InfoScreen.API.View.Application;
using _2.InfoScreen.API.View.Application.Validators.Interfaces;
using _2.InfoScreen.API.View.Application.Validators.Managers;
using _3.InfoScreen.API.View.Infrastructure.Context;
using _3.InfoScreen.API.View.Infrastructure.Interfaces;
using _3.InfoScreen.API.View.Infrastructure.Repositories.EntityRepositories;
using _3.InfoScreen.API.View.Infrastructure.Services;
using _3.InfoScreen.API.View.Infrastructure.Services.Interfaces;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using AutoMapper;
using HealthChecks.UI.Client;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using _2.InfoScreen.API.View.Application.CustomHealthChecks;
using _3.InfoScreen.API.View.Infrastructure.HostedService;
using _3.InfoScreen.API.View.Infrastructure.Managers;

namespace _1.InfoScreen.API.View.Service
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Enable CORS from out frontend
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins("http://localhost:4200").AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                });
            });

            services.AddHealthChecks()
                .AddCheck<MemoryHealthCheck>("Memory_check")
                .AddUrlGroup(uri =>
                {
                    uri.AddUri(new Uri("http://templateservice/swagger"));
                    uri.ExpectHttpCode((int)HttpStatusCode.OK);
                    uri.UseGet();
                }, "templateServiceConnection_check", HealthStatus.Unhealthy)
                .AddRedis(Configuration["REDIS_URL"], "redisConnection_check", HealthStatus.Unhealthy)
                .AddRabbitMQ(@"amqp://guest:guest@rabbitmq:5672", null, "rabbitMQConnection_check", HealthStatus.Unhealthy)
                .AddSqlServer(Configuration["Connectionstring"], "SELECT 1;", "SqlServerConnection_check", HealthStatus.Unhealthy);

            //Register the View DbContext in the ServiceCollection.
            services.AddDbContext<ViewContext>(options =>
                options.UseSqlServer(Configuration["Connectionstring"])
            );

            //Register the ViewRepository to the ServiceCollectiom
            services.AddScoped<IViewRepository, ViewRepository>();
            services.AddScoped<DataManager>();

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Configuration["REDIS_URL"];
            });

            // Here we make AutoMapper dependency injectable in the program. 
            services.AddAutoMapper(typeof(Assembly));
            // Documentation: https://dotnetcoretutorials.com/2017/09/23/using-automapper-asp-net-core/

            //Scans for handlers in project with the class Assembly
            services.AddMediatR(typeof(Assembly));

            services.AddTransient<IDbInitializer, DbInitializer>();

            //Register the Template service in the ServiceCollection.
            services.AddHttpClient<ITemplateService, TemplateService>();

            services.AddSingleton<RabbitMQConnectionService>();
            services.AddTransient<RabbitMQViewService>();

            services.AddHostedService<RabbitMQHostedService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "View API",
                    Description = "An API for storing View information and getting data based on them.",
                    Version = "v1"
                });
            });

            // Add the validation manager to the services
            services.AddScoped<IValidateManager<ViewDTO>, ValidateManager>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                // Initialize the database
                var services = scope.ServiceProvider;
                var dbContext = services.GetService<ViewContext>();
                var dbInitializer = services.GetService<IDbInitializer>();
                dbInitializer.Initialize(dbContext);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHealthChecks("/health", new HealthCheckOptions
            {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
                Predicate = registration => true
            });

            app.UseSwagger();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "View API v1");
            });

            app.UseMvc();
        }
    }
}
