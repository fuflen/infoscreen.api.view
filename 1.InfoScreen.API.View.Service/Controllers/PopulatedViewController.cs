﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2.InfoScreen.API.View.Application.Commands.PopulatedViewCommands;
using _2.InfoScreen.API.View.Application.Requests.CRUD;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Result;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace _1.InfoScreen.API.View.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PopulatedViewController : ControllerBase
    {
        private IMediator _mediator;
        public PopulatedViewController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetPopulatedView/{id}")]
        public async Task<ActionResult<Result<PopulatedViewDTO>>> GetPopulatedView([FromRoute]int id)
        {
            var response = await _mediator.Send(
                new GetPopulatedViewCommand(
                    new GetByIdRequest()
                    {
                        Id = id
                    }));

            return Ok(response);
        }

    }
}