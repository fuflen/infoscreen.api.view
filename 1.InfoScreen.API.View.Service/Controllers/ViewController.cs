﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2.InfoScreen.API.View.Application.Commands.ViewCommands;
using _2.InfoScreen.API.View.Application.Requests.CRUD;
using _4.InfoScreen.API.View.Domain.Models.DTO;
using _4.InfoScreen.API.View.Domain.Result;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace _1.InfoScreen.API.View.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViewController : ControllerBase
    {
        private IMediator _mediator;
        public ViewController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult<Result<List<ViewDTO>>>> GetAll()
        {
            Result<List<ViewDTO>> response = await _mediator.Send(new GetAllViewCommand(new GetAllRequest()));
            return Ok(response);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<ActionResult<Result<ViewDTO>>> GetById([FromRoute]int id)
        {

            var response = await _mediator.Send(new GetByIdViewCommand(new GetByIdRequest() { Id = id }));
            return Ok(response);
        }

        [HttpGet]
        [Route("GetByEndpointIdAndTemplateId/{endpointId}/{templateId}")]
        public async Task<ActionResult<Result<ViewDTO>>> GetById([FromRoute]int endpointId, [FromRoute]int templateId)
        {

            var response = await _mediator.Send(new GetByEndpointIdAndTemplateIdCommand(new GetByEndpointIdAndTemplateIdRequest() { EndpointId = endpointId, TemplateId = templateId }));
            return Ok(response);
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult<Result<ViewDTO>>> Create([FromBody]ViewDTO endpoint)
        {

            Result<ViewDTO> response =
                await _mediator.Send(
                    new CreateViewCommand(
                        new CreateRequest<ViewDTO>() { dto = endpoint }));
            return Ok(response);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<ActionResult<Result<ViewDTO>>> Delete([FromRoute] int id)
        {
            Result<ViewDTO> response = await _mediator.Send(new DeleteViewCommand(new DeleteRequest { Id = id }));
            return Ok(response);
        }

        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult<Result<ViewDTO>>> Update([FromBody] ViewDTO endpoint)
        {
            Result<ViewDTO> response = await _mediator.Send(new UpdateViewCommand(new UpdateRequest<ViewDTO> { dto = endpoint }));
            return Ok(response);
        }
    }
}